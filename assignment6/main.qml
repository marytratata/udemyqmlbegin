import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 1000
    height: 700
    visible: true
    title: qsTr("Hello World")

    id: root

    //section 6. Assignment
    Rectangle {
        id: moveRect
        color: "red"
        width: 100
        height: 100

        focus: true

        x: (parent.width / 2) - (width / 2)
        y: (parent.height / 2) - (height / 2)


        Keys.onRightPressed: moveRect.x = moveRect.x + 30
        Keys.onLeftPressed: moveRect.x = moveRect.x - 30
        Keys.onUpPressed: moveRect.y = moveRect.y - 30
        Keys.onDownPressed: moveRect.y = moveRect.y + 30
    }

   }
