import QtQuick 2.15

Item {

    property color color: "blue"
    property color hoverColor: "lightblue"
    property color clickColor: "orange"
    property color textColor: "white"
    property alias title: display
    property alias area: mousearea

    id: root

    width: 30
    height: 30

    Rectangle {
        height: root.height
        width: root.width
        color: root.color
        clip: true

        Text {
            id: display
            text: ""
            anchors.centerIn: parent
            color: root.textColor
            font.bold: true
        }

        MouseArea {
            id: mousearea
            anchors.fill: parent
            hoverEnabled: true

            onEntered: parent.color = root.hoverColor
            onExited:  parent.color = root.color

            onPressed: parent.color = root.clickColor
            onReleased: parent.color = root.hoverColor
        }

    }
}
