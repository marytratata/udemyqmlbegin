import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    //section 5. Assignment. NumberPad
    Grid {
        anchors.centerIn: parent
        spacing: 5
        rows: 5
        columns: 3

        Text {
            id: dispNum
            font.bold: true
            font.pixelSize: 25
            text: qsTr("Hello")
            width: 30
            height: 30
        }

        Rectangle {width: 30; height: 30; color: "transparent"}
        Rectangle {width: 30; height: 30; color: "transparent"}

        HoverBtn {title.text: "9";  area.onPressed: dispNum.text = title.text}
        HoverBtn {title.text: "8";  area.onPressed: dispNum.text = title.text}
        HoverBtn {title.text: "7";  area.onPressed: dispNum.text = title.text}
        HoverBtn {title.text: "6";  area.onPressed: dispNum.text = title.text}
        HoverBtn {title.text: "5";  area.onPressed: dispNum.text = title.text}
        HoverBtn {title.text: "4";  area.onPressed: dispNum.text = title.text}
        HoverBtn {title.text: "3";  area.onPressed: dispNum.text = title.text}
        HoverBtn {title.text: "2";  area.onPressed: dispNum.text = title.text}
        HoverBtn {title.text: "1";  area.onPressed: dispNum.text = title.text}
        Rectangle {width: 30; height: 30; color: "transparent"}
        HoverBtn {title.text: "0";  area.onPressed: dispNum.text = title.text}
    }
}
