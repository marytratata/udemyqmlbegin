import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 6.3

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Rectangle {
        id: rectangle
        x: 75
        y: 150
        width: 119
        height: 112
        color: "#7c7473"
        focus: true

        MouseArea {
            id: mouseArea
            anchors.fill: parent

            Connections {
                target: mouseArea
                onClicked: {
                    text1.text = "Color changed"
                    rectangle.color = "red"
}
            }

            Connections {
                target: mouseArea
                onDoubleClicked: {
                    text1.text = "click me";
                    rectangle.color = "#7c7473";
                }
            }
        }

        Text {
            id: text1
            text: qsTr("click me")
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 15
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }

    Rectangle {
        id: rectangle1
        x: 224
        y: 150
        width: 119
        height: 112
        color: "#98c68a"
        focus: true

        PropertyAnimation {
            id: animationUp
            target: rectangle1
            property: "y"
            to: 150
            duration: 500
        }

        PropertyAnimation {
            id: animationDown
            target: rectangle1
            property: "y"
            to: 75
            duration: 500
        }

        MouseArea {
            anchors.fill: parent
            anchors.rightMargin: -8
            anchors.bottomMargin: -36
            anchors.leftMargin: 8
            anchors.topMargin: 36
            onClicked: {
                if(rectangle1.y === 150) {
                    animationDown.start()
                }
                else {
                    animationUp.start()
                }
            }
        }

        Text {
            id: text2
            text: qsTr("click me")
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 15
            horizontalAlignment: Text.AlignLeft
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
